============
Slirp helper
============

Introduction
============

User-mode networking offered by libslirp can be provided by a
standalone process, that we call a "helper". The networking data is
fed through a UNIX datagram socket. The stream is connected to
libslirp slirp_input() and send_packet() callback.

Since there is interest in implementing different helpers (possibly
without using libslirp), this document describes basic options and
behaviour to follow in order to be able to substitute a helper with
another.

The helper must not daemonize itself, but it may be daemonized by the
management layer. It may also have a restricted access to the system.

File descriptors 0, 1 and 2 will exist, and have regular
stdin/stdout/stderr usage (they may have been redirected to /dev/null
by the management layer, or to a log handler).

The helper must end (as quickly and cleanly as possible) when the
SIGTERM signal is received. Eventually, it may receive SIGKILL by the
management layer after a few seconds.

The following command line options have an expected behaviour. They
are mandatory, unless explicitly said differently:

--socket-path=PATH

  This option specify the location of the Unix domain datagram socket.
  It is incompatible with --fd.

--fd=FDNUM

  When this argument is given, the helper is started with the datagram
  socket as file descriptor FDNUM. It is incompatible with
  --socket-path.

--print-capabilities

  Output to stdout the capabilities in JSON format, and then exit
  successfully. Other options and arguments should be ignored, and the
  program should not perform its normal function. The capabilities can
  be reported dynamically depending on the host capabilities.

.. code:: json

  {
    "type": "slirp-helper",
    "features": [
      "dbus-address",
      "dbus-p2p",
      "exit-with-parent",
      "migrate",
      "tftp",
      "ipv4",
      "ipv6",
      "restrict"
    ]
  }

Capabilities
============

IPv4
----

The following options are available when "ipv4" is in the feature list.

--net=IP

  Network address.

--mask=IPMASK

  Network mask.

--host=IP

  Guest visible address of the host.

--dhcp-start=IP

  The first of the 16 IPs the built-in DHCP server can assign.

--dns=IP

  Guest-visible address of the virtual nameserver.

--disable-ipv4

  Whether to disable IPv4.

IPv6
----

The following options are available when "ipv6" is in the feature list.

--prefix-ipv6=IP

  Network prefix.

--prefix-length-ipv6=LEN

  Network prefix length.

--host-ipv6=IP

  Guest visible address of the host.

--dns-ipv6=IP

  Guest-visible address of the virtual nameserver.

--disable-ipv6

  Whether to disable IPv6.

TFTP
----

The following options are available when "tftp" is in the feature list.

--tftp-root=PATH

  Activate the TFTP server and specify the root directory of the server.

--tftp-bootfile=PATH

  BOOTP filename.

D-Bus
-----

The following option is available when "dbus-address" is in the
feature list.

--dbus-address=DBUS_ADDRESS

  D-Bus address to connect to. The connection may not be to a full
  "bus", but only a 1-1 connection. Registering on the bus may thus
  fail, and should not be considered as an error.

  In order to be easily associated with its process, it is recommended
  to register the "org.freedesktop.Slirp1_$PID" bus name for the
  connection.

The following option is available when "dbus-p2p" is in the
feature list.

--dbus-p2p=DBUS_ADDRESS

  D-Bus address to listen on for peer-to-peer connections.

Migration
---------

"migrate" capability requires DBus support with
org.freedesktop.Slirp1.Helper.Save interface implemented.

The following option is also available:

--dbus-id=ID

  Set the helper identifier (must be unique among the
  org.qemu.VMState1 providers).

--dbus-incoming

  Put the helper in state where incoming data is expected through the
  DBus migration interface.

--incoming-fd=FDNUM

  FD to read from to restore the state of libslirp (the data
  previously saved via org.freedesktop.Slirp1.Helper.Save).

Restrict
--------

The following option is available when "restrict" is in the
feature list.

--restrict

  Isolate guest from host.

Exit with parent
----------------

The following option is available when "exit-with-parent" is in the
feature list.

--exit-with-parent

  Exit with parent process.

DBus interface
==============

The helper process may provide IPC through the following DBus
interface:

.. code:: xml

  <node name="/org/freedesktop/Slirp1/Helper">
    <interface name="org.freedesktop.Slirp1.Helper">
      <method name="GetInfo">
        <arg name="info" type="s" direction="out"/>
      </method>
    </interface>
  </node>


GetInfo() should return a human-readable string as formatted by
slirp_connection_info(). (in the future, a more machine readable
version of the data may be provided via a specific interface or
properties)

When "migration" is supported, org.qemu.VMState1 interface is also
implemented:

.. code:: xml

  <node name="/org/qemu/VMState1">
    <interface name="org.qemu.VMState1">
      <property name="Id" type="s" access="read"/>
      <method name="Load">
        <arg name="data" type="ay" direction="in"/>
      </method>
      <method name="Save">
        <arg name="data" type="ay" direction="out"/>
      </method>
    </interface>
  </node>

Save() should return a buffer of data that can be suitably read back
with --incoming-fd=FD when restoring the helper, or provided to
Load().

Those methods may throw errors.
